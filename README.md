# Codekunst Karlsruhe GmbH - Headless Website

## Setup
- Clone Repo: https://xxxxxx
- Add "shared"-folder in `api/` like `api/shared`
- Create `local-config.php` in `api/` with your DB credentials (You can copy the `local-config-sample.php`)
    - Default User And Pw from Homestead => User: homestead | Pw: secret
- Add Wordpress (Submodule): `git submodule update --init --recursive`
- Fetch all Version-Tags: `git fetch --all --tags --prune`
- Install Homestead Vagrant
    - If no composer-files are available: `composer require laravel/homestead --dev`
    - Otherwise: `composer install`
- Init the Homestead Vagrant: `php vendor/bin/homestead make`
- Edit the `Homestead.yaml` so it fits your needs
    - At `sites:to` change the `public` to `api`
    - Use your DB name
- Finally `vagrant up` - done 🔥

## Change Version of Wordpress
For changing the Wordpress-Version you have to check out a tag in `api/wp` dir. 
Do something like this:`cd api/wp/` and then `git checkout tags/5.0.3` (5.0.3 is a example)

## General information
* WordPress as a Git submodule in `api/wp/`
* Custom content directory in `api/content/` (cleaner, and also because it can't be in `api/wp/`)
* `wp-config.php` in `api` (because it can't be in `api/wp/`)
* All writable directories are symlinked to similarly named locations under `api/shared/`.

## Questions & Answers
**Q:** Why the `/shared/` symlink stuff for uploads?  
**A:** For local development, create `/shared/` (it is ignored by Git), and have the files live there. For production, have your deploy script (Capistrano is my choice) look for symlinks pointing to `/shared/` and repoint them to some outside-the-repo location (like an NFS shared directory or something). This gives you separation between Git-managed code and uploaded files.

**Q:** What's the deal with `local-config.php`?  
**A:** It is for local development, which might have different MySQL credentials or do things like enable query saving or debug mode. This file is ignored by Git, so it doesn't accidentally get checked in. If the file does not exist (which it shouldn't, in production), then WordPress will use the DB credentials defined in `wp-config.php`.